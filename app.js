const express = require ('express')
const bodyParser = require("body-parser")
const mongoose = require('mongoose')
const passport = require('passport')

const auth = require('./controllers/auth')
const order = require('./controllers/order')


const config = require('./config')
const app = express()

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use('/uploads',express.static('uploads'))
mongoose.connect(config.URI,{useNewUrlParser:true, useUnifiedTopology:true})
    .then(()=>console.log('MongoDB connect'))
    .catch(error => console.log(error))



app.use(passport.initialize())
require('./passportJwt')(passport)


app.use('/api/auth', auth)
app.use('/api/order', order)




module.exports = app