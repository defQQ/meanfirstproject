const express = require('express')
const router = express.Router()
const passport = require('passport')
const Order = require('../models/order.model')


const getAll = async function (req, res) {
    const query = {
        user: req.user.id
    }
    if (req.query.start) {
        query.date = {
            $gte: req.query.start
        }
    }
    if (req.query.end) {
        if (!query.date) {
            query.date = {}
        }

        query.date['$lte'] = req.query.end
    }

    if (req.query.order) {
        query.order = +req.query.order
    }

    try {
        const orders = await Order
            .find(query)
            .sort({date: -1})
            .skip(+req.query.offset)
            .limit(+req.query.limit)

        res.status(200).json(orders)

    } catch (e) {
        console.log(e)
    }
}

const create = async function (req, res) {


    try {
        const lastId = await Order.findOne().sort({idOrder: -1})
        const maxId = lastId ? lastId.idOrder + 1 : 1

        const order = await new Order({
            idOrder: maxId,
            name: req.body.name,
            phone: req.body.phone,
            address: req.body.address,
            description: req.body.description
        }).save()
        console.log('save order id: ' + maxId)
        res.status(201).json(order)
    } catch (e) {
        console.log(e)
    }
}

router.get('/', passport.authenticate('jwt', {session: false}), getAll)

router.post('/', create)


module.exports = router
