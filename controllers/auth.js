const express = require('express')
const user = require('../models/users.model')
const jwt = require('jsonwebtoken')
const config = require('../config')
const router = express.Router()

const login = async function (req, res) {

    const checkUser = await user.findOne({login: req.body.login})

    if (checkUser) {

        if (checkUser.password === req.body.password) {
            const token = jwt.sign({
                login: checkUser.login,
                userId: checkUser._id
            }, config.jwt, {expiresIn: 100})


            res.status(200).json({
                token: token
            })
        } else {
            res.status(401).send("Не верный пароль")//unauthorized
        }
    } else {
        res.status(404).send("Нет такого пользователя")
    }
}

router.post('/login', login)


module.exports = router