import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {StartModule} from "./start-page/start.module";

import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {AppRoutingModule} from "./app-routing.module";

import {AdminPageModule} from "./admin-page/admin-page.module";
import {MaterialModule} from "./material.module";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule,
    AdminPageModule,
    StartModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
