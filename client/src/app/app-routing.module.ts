import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";



const routes: Routes=[
   {path:'' ,loadChildren:()=> import('./start-page/start.module').then(m=>m.StartModule) },
  {path:'login' ,loadChildren:()=>import('./admin-page/admin-page.module').then(m=>m.AdminPageModule) }

];

@NgModule({
  imports:[
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule{

}
