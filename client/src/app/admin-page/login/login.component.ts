import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {IAuthUser} from "../auth.user.interfaces";
import {Observable, of, Subscribable, Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup
  aSub: Subscription

  constructor(private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.checkToken()
    this.form = new FormGroup({
      login: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required, Validators.minLength(5)]),
    })
  }

  checkToken() {
    const checkToken = localStorage.getItem('auth-token')
    if (checkToken !== null ) {
      this.router.navigate(['/overview'])
      this.auth.setToken(checkToken)
    }
  }

  ngOnDestroy(): void {
    if (this.aSub) this.aSub.unsubscribe()
  }

  submit() {
    console.log('Click Login')
    this.aSub = this.auth.login(this.form.value).subscribe(
      () => this.router.navigate(['/overview']),
      error => {
        this.form.enable()
      }
    )
  }


}

