import {Component, HostBinding, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-panel-layout',
  templateUrl: './panel-layout.component.html',
  styleUrls: ['./panel-layout.component.css']
})
export class PanelLayoutComponent {

   constructor(public auth:AuthService){

   }
  @HostBinding("class.menu-opened") menuOpened = false;

  toggleMenu() {
    this.menuOpened = !this.menuOpened;

  }

  closeMenu() {
    if (this.menuOpened == true)
      this.menuOpened = !this.menuOpened;
  }

  logOut(){
    this.auth.logout()
  }
}
