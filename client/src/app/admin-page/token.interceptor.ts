import {Injectable} from "@angular/core";
import {AuthService} from "./services/auth.service";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     if (this.auth.isAuth()) {
       req = req.clone({
         setHeaders: {
           Authorization: this.auth.getToken()
         }
       })
       return next.handle(req).pipe(
         catchError((error:HttpErrorResponse) => {


           return throwError(error)
         }))


     }
    return next.handle(req)
  }
  handleAuthError(error){

  }

}
