import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";


import {NgModule} from "@angular/core";
import {PanelLayoutComponent} from "./panel-layout/panel-layout.component";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'overview', component: PanelLayoutComponent, canActivate: [AuthGuard]},


]
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]

})

export class AdminRoutingModule {
}
