import {Injectable} from "@angular/core";
import {IAuthUser} from "../auth.user.interfaces";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Injectable()
export class AuthService {
  private token = null

  constructor(private http: HttpClient) {

  }

  login(user: IAuthUser): Observable<{ token: string }> {
    console.log('login start')
    return this.http.post<{ token: string }>('/api/auth/login', user)
      .pipe(
        tap(({token}) => {
            console.log('write token')
            localStorage.setItem('auth-token', token)
            this.setToken(token)
          }
        )
      )
  }

  setToken(token: string) {
    this.token = token
  }

  getToken(): string {
    return this.token
  }

  isAuth(): boolean {
    return !!this.token
  }

  logout() {
    this.setToken(null)
    localStorage.clear()
  }
}
