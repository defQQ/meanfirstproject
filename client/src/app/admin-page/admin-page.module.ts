import {NgModule, OnInit} from '@angular/core';
import {LoginComponent} from "./login/login.component";


import {CommonModule} from "@angular/common";

import {AdminRoutingModule} from './admin-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../material.module";
import {PanelLayoutComponent} from './panel-layout/panel-layout.component';
import {AuthService} from "./services/auth.service";
import {AuthGuard} from "./auth.guard";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {TokenInterceptor} from "./token.interceptor";
import {ExtendedModule, FlexModule} from "@angular/flex-layout";
import { OrdersComponent } from './panel-layout/orders/orders.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";


@NgModule({
  declarations: [
    LoginComponent,
    PanelLayoutComponent,
    OrdersComponent,


  ],
  imports: [
    AdminRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ExtendedModule,
    MatTableModule,
    MatPaginatorModule,
    FlexModule,


  ],
  providers: [
    AuthService, AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    }
  ],
  exports: []
})
export class AdminPageModule{
}

