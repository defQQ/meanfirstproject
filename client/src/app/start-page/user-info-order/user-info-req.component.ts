import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

import {MatSnackBar} from "@angular/material/snack-bar";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {IuserOrder} from "./user-info-order-interface";
import {MongoDBService} from "./mongoDB.service";
import {Subscription} from "rxjs";



@Component({
  selector: 'app-user-info-req',
  templateUrl: './user-info-req.component.html',
  styleUrls: ['./user-info-req.component.css']
})


export class UserInfoReqComponent implements OnInit {

  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  userInfoReq: FormGroup;
  private aSub: Subscription

  constructor(public _snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<UserInfoReqComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IuserOrder,
              private db: MongoDBService) {
  }


  ngOnInit() {
    this.userInfoReq = new FormGroup({
      name: new FormControl(null, Validators.minLength(4)),
      phone: new FormControl(null),
      address: new FormControl(null),
      description: new FormControl(null, Validators.minLength(5)),
    })
  }

  createOrder() {

    this.aSub = this.db.createOrder(this.userInfoReq.value).subscribe(() => this.openSnackBar(),
      error => {
        console.log(error.error.message)
      })


  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openSnackBar() {
    this._snackBar.open('Заявка отправлена', '', {
      duration: 3000,
    });
    this.dialogRef.close()
    this.aSub.unsubscribe()
  }

}
