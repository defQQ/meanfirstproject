import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {IuserOrder} from "./user-info-order-interface";
import {Injectable} from "@angular/core";
import {tap} from "rxjs/operators";


@Injectable()
export class MongoDBService {

  constructor(private http: HttpClient) {
  }

  createOrder(order: IuserOrder): Observable<any> {
    console.log('start create')

    return this.http.post('/api/order', order)


  }
}
