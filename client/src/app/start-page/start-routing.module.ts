import {RouterModule, Routes} from "@angular/router";
import {StartComponent} from "./start.component";
import {NgModule} from "@angular/core";

const routes: Routes=[
  {path:'' , component:StartComponent}]


@NgModule({
  imports:[
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class StartRoutingModule{

}
