import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {UserInfoReqComponent} from "../user-info-order/user-info-req.component";



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(UserInfoReqComponent, {
      width: '460px',
      height: '470px',
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.openSnackBar();
    });
  }


  ngOnInit(): void {
  }

}
