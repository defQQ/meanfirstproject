import { NgModule } from '@angular/core';
import { StartComponent } from './start.component';


import {ExtendedModule, FlexModule} from "@angular/flex-layout";

import {HeaderComponent} from "./header/header.component";
import {ReactiveFormsModule} from "@angular/forms";

import {AdminPageModule} from "../admin-page/admin-page.module";
import {AboutComponent} from "./about/about.component";
import {UserInfoReqComponent} from "./user-info-order/user-info-req.component";

import {MaterialModule} from "../material.module";
import {CommonModule} from "@angular/common";
import {StartRoutingModule} from "./start-routing.module";
import {MongoDBService} from "./user-info-order/mongoDB.service";
import {MatMenuModule} from "@angular/material/menu";
import {ContactComponent} from "./contact/contact.component";





@NgModule({
  declarations: [
    StartComponent,
    HeaderComponent,
    AboutComponent,
    UserInfoReqComponent,
    ContactComponent

  ],
  imports: [
    ReactiveFormsModule,
    ExtendedModule,
    FlexModule,
    AdminPageModule,
    MaterialModule,
    CommonModule,
    StartRoutingModule,
    MatMenuModule,


  ],
  providers: [
    MongoDBService
  ]
})
export class StartModule { }
