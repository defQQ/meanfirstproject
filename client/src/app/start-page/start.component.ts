import {Component, HostBinding} from '@angular/core';

@Component({
  selector: 'start-app',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent  {

  @HostBinding("class.menu-opened") menuOpened = false;

  toggleMenu(){
    this.menuOpened = !this.menuOpened;
  }
  closeMenu(){
    if(this.menuOpened==true)
      this.menuOpened = !this.menuOpened;
  }
}
