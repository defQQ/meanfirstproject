const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userSchema = new Schema({
    login: {
        type: String,
        required: true,
        unicode: true
    },
    password: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('users',userSchema)