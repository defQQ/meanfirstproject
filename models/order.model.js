const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    date: {
        type: Date,
        default: Date.now
    },
    idOrder:{
        type: Number,
        default: 1
    },
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        default: ""
    },
    description:{
        type:String,
        default:""
    },
    isArchive:{
        type: Boolean,
        default: false
    }

})

module.exports = mongoose.model('orders',orderSchema)

